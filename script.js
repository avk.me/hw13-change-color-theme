const styles = ['css/style_original.css', 'css/style_tasteless.css']
const style = document.querySelector('.style');
let count = window.localStorage.getItem('currentStyle') || 0;
style.setAttribute('href', styles[count]);

const changeThemeButton = document.querySelector('.change-theme-button');
changeThemeButton.addEventListener("click", function () {
    count++;
    if (count === styles.length) {
        count = 0;
    }
    let currentStyle = styles[count];
    style.removeAttribute('href');
    style.setAttribute('href', currentStyle);
})

window.addEventListener('beforeunload', function () {
    window.localStorage.setItem('currentStyle', count);
});